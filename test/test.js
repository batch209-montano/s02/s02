const { getCircleArea,checkIfPassed,getAverage,getSum,getDifference,div_check,isOddOrEven,reverseString } = require('../src/util.js');
//import the assert statements from our chai
const { expect,assert } = require('chai');

//test case - a condition we are testing
//it(stringExplainsWhatTheTestDoes,functionToTest)
//assert is used to assert conditions for the test to pass. If the assertion fails then the test is considered failed.
//describe() is used to create a test suite. A test suite is a group of test cases related to one another or tests the same method, data or function.

describe('test_get_area_circle_area',()=>{


    it('test_area_of_circle_radius_15_is_705.86',()=>{

        let area = getCircleArea(15);
        assert.equal(area,706.86);

    });

    it('test_area_of_circle_radius_300_is_282744', ()=>{

        let area = getCircleArea(300);
        expect(area).to.equal(282744);

    });

});

describe('test_check_if_passed', ()=>{

    it('test_20_out_of_30_if_passed',()=>{

        let isPassed = checkIfPassed(25,30)
        assert.equal(isPassed,true);

    });

    it('test_30_out_of_50_is_not_passed',()=>{

        let isPassed = checkIfPassed(30,50)
        assert.equal(isPassed,false);

    });
   

});

describe('test_get_average', ()=>{

    it('test_average_of_numbers_80_82_84_86_is_83',()=>{

        let numAvg = getAverage(80,82,84,86)
        assert.equal(numAvg,83);

    });

    it('test_average_of_numbers_70_80_82_84_is_79',()=>{

        let numAvg = getAverage(70,80,82,84)
        assert.equal(numAvg,79);

    });

});

describe('test_get_sum', ()=>{

    it('test_sum_of_numbers_15_and_30_is_45',()=>{

        let sum = getSum(15,30)
        assert.equal(sum,45);

    });

    it('test_sum_of_numbers_25_and_50_is_75',()=>{

        let sum = getSum(25,50)
        assert.equal(sum,75);

    });

});

describe('test_get_difference', ()=>{

    it('test_difference_of_numbers_75_and_40_is_35',()=>{

        let diff = getDifference(75,40)
        assert.equal(diff,35);

    });

    it('test_difference_of_numbers_125_and_50_is_75',()=>{

        let diff = getDifference(125,50)
        assert.equal(diff,75);

    });

});

//2. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.

describe('test_to_check_a_number_if_it_is_divisible_by_5_or_7.', ()=>{

    it('test_if_a_number_is_divisible_by_5',()=>{

        let divisible = div_check(35)
        assert.equal(divisible,true)

    });

    it('test_if_a_number_is_divisible_by_7',()=>{

        let divisible = div_check(49)
        assert.equal(divisible,true)

    });

    it('test_if_a_number_is_divisible_by_5',()=>{

        let divisible = div_check(55)
        assert.equal(divisible,true)

    });

    it('test_if_a_number_is_divisible_by_7',()=>{

        let divisible = div_check(63) 
        assert.equal(divisible,true)
        
       
       
    });

});

//4. Create 4 test cases in a new test suite in test.js that would check if the functionality of isOddOrEven() is correct.

describe('test_to_check_a_number_if_is_odd_or_even.', ()=>{

    it('test_if_a_number_25_is_odd',()=>{

        let oddOrEven = isOddOrEven(25)
        assert.equal(oddOrEven,'odd');
       
    });

    it('test_if_a_number_24_is_even',()=>{

        let oddOrEven = isOddOrEven(24)
        assert.equal(oddOrEven,'even')

    });

    it('test_if_a_number_26_is_even',()=>{

        let oddOrEven = isOddOrEven(26)
        assert.equal(oddOrEven,'even')

    });

    it('test_if_a_number_27_is_odd',()=>{

        let oddOrEven = isOddOrEven(27)
        assert.equal(oddOrEven,'odd')

    });

});

//Stretch Goal

//Create 4 test cases in a new test suite in test.js that would check if the functionality of reverseString() is correct.

describe('test_to_check_if_the_functionality_of_reverseString()_is_correct', ()=>{

    it('test_if_a_string_idol_would_be_lodi',()=>{

        let stringInReverse = reverseString('idol')
        assert.equal(stringInReverse,'lodi')
        console.log(stringInReverse);  
    });

    it('test_if_a_string_smoke_would_be_ekoms',()=>{

        let stringInReverse = reverseString('smoke')
        assert.equal(stringInReverse,'ekoms')
        console.log(stringInReverse); 
    });

    it('test_if_a_string_putamalas_would_be_salamatup',()=>{

        let stringInReverse = reverseString('putamalas')
        assert.equal(stringInReverse,'salamatup')
        console.log(stringInReverse);
    });

    it('test_if_a_string_gobulacan_would_be_nacalubog',()=>{

        let stringInReverse = reverseString('gobulacan')
        assert.equal(stringInReverse,'nacalubog')
        console.log(stringInReverse);
    });


});