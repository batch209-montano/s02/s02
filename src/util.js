/* Sample Functions to Test */

function getCircleArea(radius){

    //area = pi x r^2

    return 3.1416*(radius**2);

}

function checkIfPassed(score,total){

    //score/total*100 >= 75

    return(score/total)*100 >= 75;

}

function getAverage(num1,num2,num3,num4){

    //Average = (num1+num2+num3+num4)/4

    return(num1+num2+num3+num4)/4;

}

function getSum(num1,num2){

    //Sum = num1+num2

    return num1+num2;

}

function getDifference(num1,num2){

    //Difference = num1-num2

    return num1-num2;

}
// Activity

//1. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.

function div_check(div_check){


 //a. if the number received is divisble by 5, return true.
    if (div_check % 5 == 0){

        return true;
//b. if the number received is divisble by 7, return true.
    } else if(div_check % 7 == 0){

        return true;

    }else {
//c. return false if otherwise.
        return false;
    }
}     

//3. in your util.js, create a function called isOddOrEven in util.js that check a number if is odd or even.
function isOddOrEven(oddEven){

//a. if the number received is even, return 'even'
    if (oddEven % 2 === 0) {

        return 'even';

    } else {
//b. Return 'odd' if otherwise
        return 'odd';
    }


}
   

//Stretch Goal

//Create a function called reverseString() that is able to reverse a given a string.
function reverseString(reverseString){

    return reverseString.split('').reverse().join('');
      
    }

module.exports = {
    getCircleArea: getCircleArea,
    checkIfPassed: checkIfPassed,
    getAverage: getAverage,
    getSum : getSum,
    getDifference : getDifference,
    div_check : div_check,
    isOddOrEven : isOddOrEven,
    reverseString : reverseString
}



/* 
    Mini-Activity

    Create a new test suite to test cases for the get average function

    test case 1: assert that the average of 80,82,84 and 86 is 83.

    test case 2: assert that the average of 70,80,82 and 84 is 79.

    Run your test with npm and take a screenshot of your test. Send your test in the hangouts.

    Mini-Activity #2

    Create a function called getSum which is able to receive 2 numbers and returns the sum of both arguments.

    Create a test suite to check the function result for following pairs of numbers:
        15,30
        25,50

    Create a function called getDifference which is able to receive 2 numbers and returns the difference of both arguments.  
    
    Create a test suite to check the function result for following pairs of numbers:
        75,40
        125,50

*/













